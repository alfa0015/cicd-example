FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
ADD . .
RUN yarn build

FROM nginx:1.13.12-alpine as production-stage
RUN apk --no-cache --update add build-base bash tzdata busybox-extras shadow busybox-suid coreutils
RUN adduser -D -u 1001 webapp && usermod -aG 0 webapp
RUN sed -i 's/listen[[:space:]]*80;/listen 8080;/g' /etc/nginx/conf.d/default.conf
COPY --from=build-stage /app/dist /usr/share/nginx/html
RUN chown -R 1001 /usr/share/nginx/html && chgrp -R 0 /usr/share/nginx/html && chmod -R g=u /usr/share/nginx/html &&\
    chown -R 1001 /var/cache/nginx && chgrp -R 0 /var/cache/nginx && chmod -R g=u /var/cache/nginx && \
    chown -R 1001 /var/run/ && chgrp -R 0 /var/run/ && chmod -R g=u /var/run/
USER 1001
EXPOSE 8080
CMD ["nginx", "-g", "daemon off;"]
